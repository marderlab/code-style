# Guidelines for writing code


# Do this

* Organise your code. Make a folder for your code. Organise folders by project/use. 
* Use [git](https://git-scm.com/). Learn [how to use](https://duckduckgo.com/?q=git+tutorial) git.  
* Place a README.md in your project root. Make it useful, so that an outsider with no knowledge of your code or your project can dive in: meaning they can install your code, run it, and generate output that they can compare to some example usage.
* Pick a consistent naming scheme, and stick to it. For example, `functionName.m` is a function. `test_script.m` is a script. `foo_bar` is a variable. 
* Use sensible variable names. If you're using `i` or `j`, it better be the index of a loop. 
* Self-descriptive variable names are better than shorter but obscure variable names. So `exp_column_sum` is better than `s`
* Write [functions](https://en.wikipedia.org/wiki/Functional_programming). If you're writing code that operates on something, make it a function. Clearly define what the inputs and outputs are, and what the inputs are expected to be.
* Use assertions (`assert()`). Practise [defensive programming](https://en.wikipedia.org/wiki/Defensive_programming) -- assume that [perverse individuals](https://en.wikipedia.org/wiki/Student) will try to feed your function weird inputs. 
* Separate your functions from your scripts in different folders. 
* Write modular code. It's better to have multiple files with clearly separated functions that do one small thing each than 1 big file that does a lot of stuff
* Every piece of code you write should have a short piece of text at the top telling the reader what this does, what expected inputs and outputs are
* Write code for humans, not the computer. "Code is meant to be read, and only incidentally executed." -- [Donald Knuth](https://en.wikipedia.org/wiki/Donald_Knuth) 
* Tabs or spaces. Pick one and stick to it. 
* Spacing around operators -- pick one convention and stick to it. So **either** `a=b` **or** `a = b` but not both in the same file
* If you have a lot of parameters, group them into a parameter structure (maybe call if `parameters` or `p`) and pass that around. Access individual parameters using `p.bar`. 

# Don't do this

* Don't put your code in a folder called "`my name`"
* Don't put your code in a folder that has a space in its name. If you must, create a [symlink](https://en.wikipedia.org/wiki/Symbolic_link) to that folder so you can access it without having to type a space. 
* Don't name your files "`code1.m`" or "`code_new`" or "`analysis_final`"
* Functions should not refer to **anything** except arguments. No files on disk, no global variables. 
* Your function shouldn't assume that inputs will be of a certain size, shape or type. If they do, make sure you use assertions so that they fail if expectations are not met. The worst possible thing is for functions to run without an error with unexpected inputs.
* Don't have two functions that share 90% of the code. 
* Don't use timestamps in filenames to distinguish between many versions of code. That's what `git branch` is for
* Don't have files called "`test`", "`test2`" and "`test3`". If you're doing this, you haven't learn how to use git. 
* Don't have obscure scripts that have 3 lines of code
* Don't hard-code parameters in your code. A good rule of thumb is never to have any numbers in the main body of your code. Parameters should have long descriptive names, and should be defined at the top of your function. 

## Further reading

1. [How to write unmaintainable code](https://www.thc.org/root/phun/unmaintain.html)
